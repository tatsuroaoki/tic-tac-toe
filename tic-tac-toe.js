//tic-tac-toe board exercise solution 1
let board = [
	{one: '-',
		two: 'O',
		three: '-'
	},
	{one: '-',
		two: 'X',
		three: 'O'
	},
	{one: 'X',
		two: '-',
		three: 'X'
	}
]

// update top right corner
board[0].three = 'O';